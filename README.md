# A particles simulator in project #

This is the main repo of my first C++ coding project, written in the first week ofthe Easter holidays of 2015, I've never coded in C++ before writing this project, and learnt on-the-fly C++ specifications and syntax. 

### What is this repository for? ###

* Particle Simulator based on hand coded physical engine and interface 
* Alpha v0 (basic physics and fundamentals constants are coded, waiting for a better PC to work on graphical view and going forward.)

### How do I get set up? ###

* Summary of set up

At this moment, this particle simulator is only usable by hard coding the process you want to verify, my current computer is not strong enough to let me code efficiently an graphical interface, I need to wait for a new PC before coding forward.

Basics tests are already visible when launching the project. 


* Configuration

Currently, there is no options, but we'll need to be able to change differents parameters (graphical or physical) later.

* Dependencies

This Project is made without using any dependencies currently.

* Database configuration

Currently, Particle database is unexistant, every particle is hard coded at this time. (including the 6 quarks; nucleus composites (proton, neutron); hadron, mesons bosons and leptons types; atoms)

* How to run tests

Currently you can run tests by coding yourself yours and compiling->running your build

* Deployment instructions

Currently this project is far from ready to be deployed.

### Contribution guidelines ###

* Writing tests -> if your tests are not correctly performed, put them in issues pool with the expected result.
* Code review -> make pull requests, I'll watch it.