#pragma once
#include <vector>
#include <string>
#include "ConfigFile.h"
class Options
{
private:
	static SciNumber timeDelay;
	static std::string const optionFile;
	static std::vector<std::string> config;
	static int searchNode(std::string);

public:
	Options();
	~Options();
	static bool load()
	{
		ConfigFile configFile(optionFile);
		std::string section = "general";
		timeDelay = SciNumber(configFile.Value(section, "Time lapse"));
	};
	static SciNumber getTimeDelay()
	{
		return timeDelay;
	}
};

