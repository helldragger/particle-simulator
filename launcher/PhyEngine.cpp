#include "stdafx.h"
#include <vector>

void PhyEngine::Debug()
{
	std::cout << "Base Particle: ";
	Particle().test();
	std::cout << "Quark Particle: ";
	Quark(std::string("up")).test();
	std::cout << "Lepton Particle: ";
	Lepton(std::string("electron")).test();
	std::cout << "--Proton with modified speed--";

	return;
}


PhyEngine::PhyEngine()
	: time(0)
	, particules(0)
{
	engine = *this;
}


PhyEngine::~PhyEngine()
{
}

SciNumber PhyEngine::getTime() const
{
	return SciNumber(time * Options::getTimeDelay().getReal());
}

/*
void PhyEngine::update()
{
	std::vector<Particle> _temp(0);
	for (int i = 0; i != particules.size(); i++)
	{
		Particle p = particules[i];
		if (p.getTempsPropre() >= p.getLifeTime())
		{
			p.desintegrer();
		}
		else
		{
			p.deplacer();
			_temp.push_back(p);
		}
	}
	particules.clear();
	particules = _temp;
}
*/

PhyEngine PhyEngine::engine = PhyEngine();