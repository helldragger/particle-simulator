#include "stdafx.h"
#include "Particle.h"
#include <string>

//PARTICULE

Particle::Particle(std::string &const name, Size size)
	: Entity()
	, m_charge(0) //en e
	, m_mass(0) //en electron Volt (eV)/c�
	, m_Size(size)
	, m_name(name)
{};

Particle::Particle()
	: Entity()
	, m_charge(0) //en e
	, m_mass(0) //en electron Volt (eV)/c�
	, m_Size(Size::Undefined)
	, m_name("template particle")
{
}

std::string Particle::getName() const
{
	return m_name;
}

Particle::Size Particle::getSize() const
{
	return m_Size;
}


SciNumber Particle::getCharge() const
{
	return m_charge;
};

SciNumber Particle::getMass() const
{
	return m_mass;
};

SciNumber Particle::interEM(SciNumber &const E)
{
	return getMass() * getCharge()*E;
}

SciNumber Particle::interFaible(Particle &const p)
{
	return getMass() * getCharge();
}

SciNumber Particle::interForte(Particle &const p)
{
	return getMass() * getCharge();
}

SciNumber Particle::interGrav(Particle &const p)
{
	return getMass() * p.getMass();
}


std::ostream& operator<<(std::ostream & flux, Particle &const p)
{
	flux << "Type: " << p.getName() << "  Size: " << p.getSize() << "\n"
		<<  "  Masse: " << p.getMass();
	flux << "\n" << "Vitesse: " << p.getSpeed()
		<< "\n" << "Temps propre: " << p.getTempsPropre()
		<< "\n" << "Charge: " << p.getCharge();
	flux << "\n  Energies:"
		<< "\n\t Ec = " << p.getCineEnergy()
		<< "\n\t Em = " << p.getMecaEnergy()
		<< "\n\t ETotale = " << p.getTotalEnergy();

	flux << "\n\n";
	return flux;


}


void Particle::test()
{
	std::cout << *this;
}



SciNumber Particle::getCineEnergy() const
{
	// Vvitesse = sqrt( Vx� + Vy� + Vz� ) 
	SciNumber c = constants::c;
	// Ec = ( Y - 1 ).m.c� o� Y = 1/sqrt(1- v�/c�)

	return SciNumber(((1 / sqrt(1 - (pow(getSpeed().getReal(), 2) / pow(c.getReal(), 2)))) - 1) * m_mass.getReal() * pow(c.getReal(), 2));
}

SciNumber Particle::getMecaEnergy() const
{
	SciNumber c = constants::c;
	return SciNumber(m_mass.getReal() * pow(c.getReal(), 2));
}

SciNumber Particle::getTotalEnergy() const
{
	return getMecaEnergy() + getCineEnergy();
}

Particle::~Particle()
{
}

SciNumber Particle::getLifeTime() const
{
	return m_lifeTime;
};

SciNumber Particle::getTempsPropre() const
{
	return m_tempsPropre;
};

SciNumber Particle::desintegrer()
{
	return getTotalEnergy();
}

void Particle::setCharge(SciNumber &const nmb)
{
	m_charge = nmb;
};


void Particle::setMass(SciNumber &const nmb)
{
	m_mass = nmb;
};


//QUARK

Quark::Quark(std::string &const name)
	: Particle(name, Size::Sub_Atomic)
{
	constants::quark type = constants::getQuarkByName(getName());
	setMass(constants::getMass(type));
	setCharge(constants::getCharge(type));

}

// LEPTON

Lepton::Lepton(std::string &const name)
	: Particle(name, Size::Sub_Atomic)
{
	constants::lepton type = constants::getLeptonByName(getName());
	setMass(constants::getMass(type));
	setCharge(constants::getCharge(type));
}

// HADRON

Hadron::Hadron(std::string &const name)
	: Particle(name, Size::Sub_Atomic)
{
};

// MESON

Meson::Meson(std::string &const name, std::vector<Quark> &const val)
	: Hadron(name)
	, m_quark(val[0])
	, m_antiquark(val[1])
{
};


// BARYON

Baryon::Baryon(std::string &const name, std::vector<Quark> &const val)
	: Hadron(name)
	, m_q1 (val[0])
	, m_q2 (val[1])
	, m_q3 (val[2])
{

};

// PROTON

Proton::Proton()
	: Nucleon(std::string("proton"))
{
};

// NUCLEON

Nucleon::Nucleon(std::string &const name)
	: m_val(constants::getNuclVal(constants::getNuclByName(name)))
	, Baryon(name, m_val)
{

}

// NUAGE

Nuage::Nuage(int &const nb)
	: m_nb_electrons(nb)
{

};

// NOYAU

Noyau::Noyau(int &const nb_pro, int &const nb_neu)
	: m_nb_protons(nb_pro)
	, m_nb_neutrons(nb_neu)
{

};

void Noyau::setNeutrons(int &const nb)
{
	m_nb_neutrons = nb;
}

void Noyau::setProtons(int &const nb)
{
	m_nb_protons = nb;
}

// ATOME 

Atome::Atome(Noyau &const noyau, Nuage &const nuage)
	: m_nuage(nuage)
	, m_noyau(noyau)
{
};



