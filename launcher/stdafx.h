// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <string>


// TODO: reference additional headers your program requires here
#include "Math.h"

#include "SciNumber.h"
#include "PhyEngine.h"
#include "Options.h"
#include "ConfigFile.h"
#include "Chameleon.h"
#include "Particle.h"
#include "constants.h"