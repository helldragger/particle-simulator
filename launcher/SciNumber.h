#pragma once
#include "Math.h"
#include <iostream>
#include <string>
class SciNumber
{
public:
	// correspond to a value in relation A = a * 10^b
	long double value;
	// correspond to the b value in relation A = a * 10^b
	long int exponent;


	SciNumber(long double val, int exp);
	SciNumber(long double original);
	SciNumber();
	~SciNumber(){};
	
	
	void Decompose(long double originalvalue);

	void setValue(long double val);
	void setExponent(int exp);
	long double getReal() const;
	
	void Test();
	friend bool operator>(SciNumber &, SciNumber &);
	friend bool operator<(SciNumber &, SciNumber &);
	friend bool operator>=(SciNumber &, SciNumber &);
	friend bool operator<=(SciNumber &, SciNumber &);
	friend bool operator==(SciNumber &, SciNumber &);

	friend SciNumber operator+(SciNumber &, SciNumber &);
	friend SciNumber operator+(int &, SciNumber &);
	friend SciNumber operator-(SciNumber &, SciNumber &);
	friend SciNumber operator-(int &, SciNumber &);
	friend SciNumber operator*(SciNumber &, SciNumber &);
	friend SciNumber operator*(int &, SciNumber &);
	friend SciNumber operator/(SciNumber &, SciNumber &);
	friend SciNumber operator%( SciNumber &,  SciNumber &);
	friend std::ostream& operator<<( std::ostream &, const SciNumber &);
};