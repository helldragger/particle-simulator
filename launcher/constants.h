#pragma once
#include <vector>

namespace constants
{
	

	SciNumber const PI = SciNumber(3.141592653, 1);
	SciNumber const c = SciNumber(2.99792458, 8); // en m.s-1
	SciNumber const h = SciNumber(4.1356692, -15); // en eV.s
	SciNumber const hbarre = SciNumber(6.582121, -16); //en eV.s
	SciNumber const G = SciNumber(6.67259, -11); // en m3.kg-1.s-2
	SciNumber const k = SciNumber(8.617385, -5); // en eV.K-1
	SciNumber const R = SciNumber(8.314510, 0); // en J.mol-1.K-1 
	SciNumber const Na = SciNumber(6.0221, 23); // en mol-1
	SciNumber const e = SciNumber(1.60217733, -19); // en C
	SciNumber const � = SciNumber(4 * PI.getReal(), -7); // en N/A�
	SciNumber const E = SciNumber(8.854187817, -12); // en F/m
	SciNumber const K = SciNumber(8.987552, 9); // en N.m�/C�
	SciNumber const F = SciNumber(96485.309, 0); // en C/mol
	SciNumber const me = SciNumber(0.51099906, 0); // en MeV/c�
	SciNumber const mp = SciNumber(938.27231, 0); // en MeV/c�
	SciNumber const mn = SciNumber(939.56563, 0); // en MeV/c�
	SciNumber const u = SciNumber(931.49432, 0); // en MeV/c�
	SciNumber const o = SciNumber(5.67051, -8); // en W/ m�.K4
	SciNumber const �b = SciNumber(5.788382, -5); // en eV/T
	SciNumber const QFlux = SciNumber(2.067834, -15); // en T/m�
	SciNumber const a0 = SciNumber(0.529177249, -10); // en m
	SciNumber const atm = SciNumber(101325, 0); // en Pa
	SciNumber const b = SciNumber(2.897756, -3); // en m.K



	static SciNumber fLorentz(SciNumber & const vitesse)
	{
		SciNumber result(
			1 / sqrt(1 - (pow(vitesse.getReal(), 2) / pow(c.getReal(), 2))));
		return result;
	}

	
};

