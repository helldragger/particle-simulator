#pragma once
#include <vector>
#include "Particle.h"

class PhyEngine
{
private:
	std::vector<Particle> particules;
	long time = 0;


public:
	static PhyEngine engine;
	static PhyEngine getEngine()
	{
		return engine;
	}
	PhyEngine();
	~PhyEngine();
	void Debug();
	void addParticle(Particle &const);
	void removeParticle(int);
	void update();
	SciNumber getTime() const;
};

