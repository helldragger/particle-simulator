#include "stdafx.h"

SciNumber::SciNumber(long double val, int exp)
	: exponent(exp)
	, value(val)
{
};

SciNumber::SciNumber(long double original)
	: exponent(0)
	, value(0.0)
{
	Decompose(original);
};

SciNumber::SciNumber()
	: exponent(0)
	, value(0.0)
{
};

void SciNumber::setValue(long double val)
{
	value = val;
}

void SciNumber::setExponent(int exp)
{
	exponent = exp;
};

long double SciNumber::getReal() const
{
	return (value * pow(10.0, exponent));
};

void SciNumber::Decompose(long double originalvalue)
{
	long double calcul = originalvalue;
	if (originalvalue != 0.0)
	{
		if (originalvalue < 0)
			calcul = -calcul;

		exponent = int(floor(log10(calcul)));
		value = originalvalue / pow(10.0, exponent);
	}
	else
	{
		exponent = 0;
		value = 0.0;
	}

	return;
};


SciNumber operator+(SciNumber &nmb1, SciNumber &nmb2)
{
	SciNumber result(0.0);
	long double value = nmb1.getReal() + nmb2.getReal();
	result.Decompose(value);
	return result;
}

SciNumber operator+(int &nmb1, SciNumber &nmb2)
{
	SciNumber result(0.0);
	long double value = nmb1 + nmb2.getReal();
	result.Decompose(value);
	return result;
}

SciNumber operator-( SciNumber &nmb1,  SciNumber &nmb2)
{
	SciNumber result(0.0);
	long double value = nmb1.getReal() - nmb2.getReal();
	result.Decompose(value);
	return result;
}

SciNumber operator-(int &nmb1, SciNumber &nmb2)
{
	SciNumber result(0.0);
	long double value = nmb1 - nmb2.getReal();
	result.Decompose(value);
	return result;
}


SciNumber operator*( SciNumber &nmb1,  SciNumber &nmb2)
{
	SciNumber result(0.0);
	result.value = nmb1.value * nmb2.value;
	result.exponent = nmb1.exponent + nmb2.exponent;
	result.Decompose( result.getReal());
	return result;
}

SciNumber operator*(int &nmb1, SciNumber &nmb2)
{
	SciNumber result(0.0);
	SciNumber nb1(nmb1);
	result.value = nb1.value * nmb2.value;
	result.exponent = nb1.exponent + nmb2.exponent;
	result.Decompose(result.getReal());
	return result;
}

SciNumber operator/( SciNumber &nmb1,  SciNumber &nmb2)
{
	SciNumber result(0.0);
	result.value = nmb1.value / nmb2.value;
	result.exponent = nmb1.exponent - nmb2.exponent;
	result.Decompose(result.getReal());
	return result;
}

SciNumber operator%( SciNumber &nmb1,  SciNumber &nmb2)
{
	SciNumber result(0.0);
	result.value = fmod(nmb1.value, nmb2.value);
	result.exponent = nmb1.exponent - nmb2.exponent;
	result.Decompose(result.getReal());
	return result;
}

bool operator>(SciNumber &nmb1, SciNumber &nmb2)
{
	if (nmb1.exponent != nmb2.exponent)
		return (nmb1.exponent > nmb2.exponent);
	else
		return (nmb1.value > nmb2.value);
}

bool operator<(SciNumber &nmb1, SciNumber &nmb2)
{
	if (nmb1.exponent != nmb2.exponent)
		return (nmb1.exponent < nmb2.exponent);
	else
		return (nmb1.value < nmb2.value);
}

bool operator==(SciNumber &nmb1, SciNumber &nmb2)
{
	return (nmb1.exponent == nmb2.exponent && nmb1.value == nmb2.value);
}

bool operator>=(SciNumber &nmb1, SciNumber &nmb2)
{
	return !(nmb1 < nmb2);
}

bool operator<=(SciNumber &nmb1, SciNumber &nmb2)
{
	return !(nmb1 > nmb2);
}

std::ostream& operator<<( std::ostream & os, const SciNumber & nmb)
{
	os << nmb.value << "*10^" << nmb.exponent;
	return os;
}

void SciNumber::Test()
{
	SciNumber nmb1 = SciNumber(465.0);
	SciNumber nmb2 = SciNumber(0.0054);

	std::cout << "\n nombre 1: " << nmb1;
	std::cout << "\n nombre 2: " << nmb2;
	std::cout << "\n addition: " << (nmb1 + nmb2);
	std::cout << "\n soustraction: " << (nmb1 - nmb2);
	std::cout << "\n multiplication: " << (nmb1 * nmb2);
	std::cout << "\n division: " << (nmb1 / nmb2);
	std::cout << "\n modulo: " << (nmb1 % nmb2);
	std::cout << "\n\n";

}