#pragma once
#include <vector>

class Entity
{
public:
	Entity()
	{
	};
	Entity(int * const [3], int * const [3])
	{};

	SciNumber getSpeed() const { return SciNumber(-1, 0); }; // get Vitesse (m.s-1)
	std::vector<int> getPosition() const; // get position (coordonn�es)
	std::vector<int> getDistance(); // get distance A-B (Vab)

	void setSpeed(int[3]);
	void setPosition(int[3]);


	void deplacer(); // calcul des coordonn�es avec le temps du moteur incr�ment� (equation horaire)
	void collisionner(Entity const &); // calcul de l'energie de la collision, desintegration si possible et creation nouvelles particules
};


class Particle : public Entity
{
public:

	enum Size
	{
		Macroscopic = 1
		, Microscopic
		, Sub_Atomic
		, Undefined
	};
	
	Particle(std::string & const, Size const);
	Particle();
	~Particle();
	void setCharge(SciNumber & const);
	void setMass(SciNumber & const);
	
	SciNumber desintegrer(); // calcul de l'energie lib�r�e lors de la desintegration
	SciNumber getTotalEnergy() const; // get Total energy (J)
	SciNumber getMecaEnergy() const; // get Mecanic energy (J)
	SciNumber getCineEnergy() const; // get cinetic energy (J)
	SciNumber getLifeTime() const; // get temps propre max
	SciNumber getTempsPropre() const; // get temps propre parcouru
	std::string getName() const; // get particle name (string)
	Size getSize() const; // get particle size (Size enum)
	SciNumber getCharge() const; // get charge (eV)
	SciNumber getMass() const; // get mass (eV/c�) 

	void test(); // teste l'initialisation des classees deriv�es
	SciNumber interForte(Particle &const); // force d'interaction forte avec l'autre particule (renvoie en J)
	SciNumber interFaible(Particle &const); // force d'interaction faible avec l'autre particule (renvoie en J)
	SciNumber interEM(SciNumber &const); // force d'interaction EM avec la force d'un champ (renvoie en J)
	SciNumber interGrav(Particle &const); // force d'interaction gravitationnelle avec l'autre particule (renvoie en J)

	friend std::ostream& operator<<(std::ostream & , Particle &const);

private:
	SciNumber m_tempsPropre =  SciNumber(0); // temps de vie relativiste
	SciNumber m_lifeTime = SciNumber(1.0, -4); // temps de vie max
	SciNumber m_eC; // energie cinetique en J
	SciNumber m_eM; // energie mecanique en J
	SciNumber m_eT; // energie totale en J
	SciNumber m_charge; //en eV
	SciNumber m_mass; //en electron Volt (eV)/c�
	Size m_Size; // type de particule
	std::string m_name; // nom de la particule
};



class Quark : public Particle
{
public:
	Quark(std::string &const);
};

class Lepton : public Particle
{
public:
	Lepton(std::string &const);
};

class Hadron : public Particle
{
public:
	Hadron(std::string &const name);
};

class Meson : public Hadron // Bosoniques
{
public:
	Meson(std::string &const, std::vector<Quark> &const);

private:
	Quark m_quark, m_antiquark;
};

class Baryon : public Hadron // Fermioniques
{
public:
	Baryon(std::string &const, std::vector<Quark> &const);

private:
	Quark m_q1, m_q2, m_q3;
};


class Nucleon : public Baryon
{
public:
	Nucleon();
	Nucleon(std::string &const);
private:
	std::vector<Quark> m_val;
};


class Proton : public Nucleon
{
public:
	Proton();
};
namespace constants
{
	static enum nucl
	{
		proton = 0
		, neutron = 1
	};

	static SciNumber getCharge(nucl type)
	{
		switch (type)
		{
		case proton:
			return SciNumber(1.0);
		case neutron:
			return SciNumber(0.0);
		}
	}

	static SciNumber getMass(nucl type)
	{
		return (type == nucl::proton) ? SciNumber(938.272, 6) : SciNumber(939.5654, 6);
	}; // return appropriate nucleus mass

	static std::vector<Quark> getNuclVal(nucl type)
	{
		if (type == nucl::proton)
		{
			std::vector<Quark> result({ Quark(std::string("up")), Quark(std::string("up")), Quark(std::string("down")) });
			return result;
		}
		else
		{
			std::vector<Quark> result({ Quark(std::string("up")), Quark(std::string("down")), Quark(std::string("down")) });
			return result;
		}
	};

	static nucl getNuclByName(std::string name)
	{
		if (name.find("proton") != std::string::npos)
			return nucl::proton;
		else
			return nucl::neutron;
	}

	static enum boson
	{
		photon = 0
		, gluon
		, Z
		, Wmin
		, Wpls
	};

	static SciNumber getCharge(boson type)
	{
		switch (type)
		{
		case photon:
		case gluon:
		case Z:
			return SciNumber(0.0);
		case Wmin:
			return SciNumber(-1.0);
		case Wpls:
			return SciNumber(1.0);
		}
	}

	static SciNumber getMass(boson type)
	{
		switch (type)
		{
		case photon:
		case gluon:
			return SciNumber(0.0, 0); // theoric = 0
		case Z:
			return SciNumber(91.1876, 9); // 91.1876 GeV/c�
		case Wmin:
		case Wpls:
			return SciNumber(80.403, 9); // 80.403 GeV/c�
		}
	}; // return appropriate nucleus mass

	static boson getBosonByName(std::string name)
	{
		if (name.find("photon") != std::string::npos)
			return boson::photon;
		else if (name.find("gluon") != std::string::npos)
			return boson::gluon;
		else if (name.find("Z") != std::string::npos)
			return boson::Z;
		else if (name.find("W-") != std::string::npos)
			return boson::Wmin;
		else
			return boson::Wpls;
	}

	static enum quark
	{
		up = 1
		, charm
		, top
		, down
		, strange
		, bottom
	};

	static SciNumber getCharge(quark type)
	{
		switch (type)
		{
		case up:
		case charm:
		case top:
			return SciNumber(1 / 2);
		case down:
		case strange:
		case bottom:
			return SciNumber(-1 / 3);
		}
	}

	static SciNumber getMass(quark type)
	{
		switch (type)
		{
		case up:
			return SciNumber(2.3, 6); // 2.3 MeV/c�
		case charm:
			return SciNumber(1.275, 9); // 1.275 GeV/c�
		case top:
			return SciNumber(173.07, 9); // 173.07 GeV/c�;
		case down:
			return SciNumber(4.8, 6); // 4.8 MeV/c�
		case strange:
			return SciNumber(95.0, 6); // 95 MeV/c�
		case bottom:
			return SciNumber(4.18, 9); // 4.18 GeV/c�;
		}
	}

	static quark getQuarkByName(std::string name)
	{
		if (name.find("up") != std::string::npos)
			return quark::up;
		else if (name.find("charm") != std::string::npos)
			return quark::charm;
		else if (name.find("top") != std::string::npos)
			return quark::top;
		else if (name.find("down") != std::string::npos)
			return quark::down;
		else if (name.find("strange") != std::string::npos)
			return quark::strange;
		else
			return quark::bottom;
	}

	static enum lepton
	{
		electron = 1
		, muon
		, tau
		, n_electron
		, n_muon
		, n_tau
	};

	static SciNumber getCharge(lepton type)
	{
		switch (type)
		{
		case electron:
		case muon:
		case tau:
			return SciNumber(-1.0);
		case n_electron:
		case n_muon:
		case n_tau:
			return SciNumber(0.0);
		}
	}

	static SciNumber getMass(lepton type)
	{
		switch (type)
		{
		case electron:
			return (SciNumber(0.511, 6)); // 0.511 MeV/c�
		case muon:
			return (SciNumber(105.7, 6)); // 105.7 MeV/c�
		case tau:
			return (SciNumber(1.777, 9)); // 1.777 GeV/c�
		case n_electron:
			return (SciNumber(2.2, 0)); // 2.2 eV/c�
		case n_muon:
			return (SciNumber(0.17, 6)); // 0.17 MeV/c�
		case n_tau:
			return (SciNumber(15.5, 6)); // 15.5 MeV/c�
		}
	}

	static lepton getLeptonByName(std::string name)
	{
		if (name.find("electron") != std::string::npos)
			return lepton::electron;
		else if (name.find("muon") != std::string::npos)
			return lepton::muon;
		else if (name.find("tau") != std::string::npos)
			return lepton::tau;
		else if (name.find("electron neutrino") != std::string::npos)
			return lepton::n_electron;
		else if (name.find("muon neutrino") != std::string::npos)
			return lepton::n_muon;
		else
			return lepton::n_tau;
	}

}


class Noyau
{
private:
	int m_nb_protons, m_nb_neutrons;
public:
	Noyau(int &const, int &const);

	void setProtons(int &const);
	void setNeutrons(int &const);

	SciNumber getMass() { return SciNumber(m_nb_protons) * constants::getMass(constants::nucl::proton) + SciNumber(m_nb_neutrons) * constants::getMass(constants::nucl::neutron); }
	int getCharge() { return m_nb_protons; }

};

class Nuage
{
private:
	int m_nb_electrons;
public:
	Nuage(int &const);

	int getCharge() { return -m_nb_electrons; }
	SciNumber getMass() { return SciNumber(m_nb_electrons) * constants::getMass(constants::lepton::electron); }
};

class Atome
{
private:
	Noyau m_noyau;
	Nuage m_nuage;
public:
	Atome(Noyau &const);
	Atome(Noyau &const, Nuage &const);

	SciNumber getCharge() { return SciNumber(m_noyau.getCharge() + m_nuage.getCharge()); }
	SciNumber getMass() { return m_noyau.getMass() + m_nuage.getMass(); }
};